const http = require('http');
const commandLineArgs = require('command-line-args');

const options = [
  { name: 'port', alias: 'p', type: Number },
];

const args = commandLineArgs(options);
const PORT = args.port || 3000;

let requestCount = 0;

const server = http.createServer((req, res) => {
  if (req.url === '/') requestCount++;
  res.end(JSON.stringify({
    message: 'Request handled successfully',
    requestCount,
  }));
});

server.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
